
   
var doughnutCommon ={
    init: function(){
        var that = this;

        var colourtheme = {
            "spectrum" :  ["#e87424", "#ebb700", "#007c92", "#7FFF00", "#6495ED", "#00008B", "#006400", "#FF4500"]
        };

        $('[data-doughnut-pie-common="true"]').each(function(index) {
            that.bindInputEvents($(this));
            var holder = $(this)[0];
            var data = that.generateData(holder);

            console.log("data", data);      
            var theme = $(this).data("theme");
            var colours = colourtheme[theme];
            var radius = 64;    
            that.buildchart(holder, index, data, colours, radius);          
        });
    },
    bindInputEvents: function(holder){
        var that = this;
        holder.find('.updatechart').click(function(e) {
            e.preventDefault();
            that.updatechart(holder);
        });

        holder.find('.dataset li input').keyup(function() {
            $(this).parent().parent().find('.value').text($(this).val());
        });         
    },
    generateData: function(holder){
        var that = this;

        var json = new Array();

        var keysObj = {};
        var legendsarray = new Array();

                $(holder).find('.dataset li').each(function(index) {

                      var key = $(this).find('.key').text();
                      var value = $(this).find('.value').text();

                        //__key obj
                        var localKeyObj = {};
                        localKeyObj[key] = "";                

                        //__legend array build
                        var stream = {
                            "name" : key,
                            "population" : value
                        }

                      jQuery.extend(keysObj, localKeyObj);

                      legendsarray.push(stream);      
                });     

                var obj ={
                    "legends": legendsarray
                };

                jQuery.extend(obj, keysObj);

                json.push(obj);     
                return json;
    },
    updatechart: function(holder){
        var that = this;

        console.log("holder", holder);

        var data = that.generateData(holder);
        console.log("data hold", data);

        var pieId = holder.find(".pie").attr("id");


        //draw arc paths
        //var svg = d3.select('#'+pieId);

        var arc_group = d3.select('#'+pieId+' > .sliceholder');



        var color = d3.scale.ordinal();

        var arc = d3.svg.arc()
            .outerRadius(64)
            .innerRadius(64 - 20);      

        var pie = d3.layout.pie()
            .sort(null)
            .value(function(d) { return d.population; });
        paths = arc_group.selectAll("path").data(pie(data[0].legends));


        paths.enter().append("svg:path")
            .attr("class", "arc")
            .attr("d", arc)
            .style("fill", function(d) { 

                console.log("d", d);
                return color(d.name); 

            })
            .transition()
            .duration(500);
            //.attrTween("d", arcTween);

        paths
            .transition()
            .duration(500)
            .attrTween("d", arcTween);

        paths.exit()
            .transition()
            .duration(500)
            //.attrTween("d", arcTween);
            .remove();




        function arcTween(d) {
          var i = d3.interpolate(this._current, d);
          this._current = i(0);
          return function(t) { return arc(i(t)); };
        }


    },
    buildchart: function(holder, index, data, colours, radius){
        var padding = 10;

        var color = d3.scale.ordinal()
            .range(colours);

        var arc = d3.svg.arc()
            .outerRadius(radius)
            .innerRadius(radius - 20);

        var pie = d3.layout.pie()
            .sort(null)
            .value(function(d) { return d.population; });


        color.domain(
            d3.keys(data[0]).filter(function(key) {
                return key !== "legends"; 
            })
        );      

        var svg = d3.select(holder).selectAll(".pie")
            .data(data)
            .enter().append("svg")
            .attr("class", "pie")
            .attr("id", "pie"+index)
            .attr("width", radius * 3)
            .attr("height", radius * 2)
            .append("g")
            .attr("class", "sliceholder")
            .attr("transform", "translate(" + radius + "," + radius + ")");

        svg.selectAll(".arc")
            .data(function(d) { return pie(d.legends); })
            .enter().append("path")
            .attr("class", "arc")
            .attr("d", arc)
            .each(function(d) { this._current = d; })
            .style("fill", function(d) { return color(d.data.name); });

        var legend = d3.select(holder).append("svg")
            .attr("class", "legend")
            .attr("width", radius * 1.7)
            .attr("height", radius * 3)
            .selectAll("g")
            .data(color.domain().slice().reverse())
            .enter().append("g")
            .attr("transform", function(d, i) { return "translate(0," + i * 25 + ")"; });

        legend.append("rect")
            .attr("width", 18)
            .attr("height", 18)
            .style("fill", color);

        legend.append("text")
            .attr("x", 24)
            .attr("y", 9)
            .attr("dy", ".35em")
            .text(function(d) { return d; });   

        var totalUnits = svg.append("svg:text")
            .attr("class", "units")
            .attr("dy", 10)
            .attr("text-anchor", "middle") // text-align: right
            .text("Marks Chart");
    }
};
    
    
    
    $(document).ready(function() {
        doughnutCommon.init();//iniate doughnut read 
    });
      